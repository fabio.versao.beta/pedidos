<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

$router->group(['prefix' => 'pedido', 'middleware' => ['cors']], function () use ($router) {
    $router->get('/', 'PedidosController@index');
    $router->post('/', 'PedidosController@store');
    $router->put('/{id}', 'PedidosController@delivery');
    $router->put('/giveback/{id}', 'PedidosController@giveback');
    $router->delete('/{id}', 'PedidosController@destroy');
    $router->post('/start', 'PedidosController@start');
    $router->post('/buscar', 'PedidosController@buscar');
    $router->get('/buscar_concluido', 'PedidosController@buscar_concluido');
});

$router->group(['prefix' => 'operacoes'], function () use ($router) {
    $router->post('soma', 'OperacoesController@soma');
    $router->post('troco', 'OperacoesController@troco');
});

$router->group(['prefix' => 'carrinho', 'middleware' => ['cors']], function () use ($router) {
    $router->get('/', 'CarrinhoController@index');
    $router->get('/{id}', 'CarrinhoController@findByPedido');
    $router->post('/', 'CarrinhoController@store');
    $router->delete('/', function(){
        echo "carrinho: -1 item";
    });
    $router->put('/', function(){
        echo "carrinho: 1 item update";
    });
    $router->delete('/limpar', function(){
        echo "carrinho: clear";
    });
});

$router->group(['prefix' => 'tipo_produto'], function () use ($router) {
    $router->get('/', 'TipoProdutoController@index');
});

$router->group(['prefix' => 'forma_pagamento', 'middleware' => ['cors']], function () use ($router) {
    $router->get('/', 'FormaPagamentoController@index');
});

$router->group(['prefix' => 'produto'], function () use ($router) {
    $router->get('/', 'ProdutosController@index');
});

$router->group(['prefix' => 'bebida'], function () use ($router) {
    $router->get('/', function(){
        echo "opções: Fanta, Coca, Sprite, Kuat";
    });
    $router->put('/', function(){
        echo "bebida indisponível";
    });
});

$router->group(['prefix' => 'molho'], function () use ($router) {
    $router->get('/', function(){
        echo "opções: pimenta, alho, mostarda, barbiecue, maionese, ketchup";
    });
    $router->put('/', function(){
        echo "molho indisponível";
    });
});

$router->group(['prefix' => 'relatorio'], function () use ($router) {
    $router->get('/quantidadePedidos', function(){
        echo "Pedidos: 800";
    });
    $router->get('/quantidadeHamburguers', function(){
        echo "Hamburguers: 500";
    });
    $router->get('/quantidadeRefri', function(){
        echo "Refri: 170";
    });
    $router->get('/quantidadeAlfajor', function(){
        echo "Alfajor: 50";
    });
    $router->get('/quantidadeMolhos', function(){
        echo "Molhos: 500";
    });
    $router->get('/valorTotalPedidos', function(){
        echo "Total R$: R$2500,00";
    });
    $router->get('/valorTotalTroco', function(){
        echo "R$ Troco utilizado: R$ R$500,00";
    });
    $router->get('/valorTotalHamburguers', function(){
        echo "R$ Hamburguers: R$ 1.500,00";
    });
    $router->get('/valorTotalRefri', function(){
        echo "R$ Refri: R$ 500,00";
    });
    $router->get('/valorTotalAlfajor', function(){
        echo "R$ Alfajor: R$ 150,00";
    });
    $router->get('/valorTotalMolhos', function(){
        echo "R$ Molhos: R$50,00";
    });
});

// $router->group(['prefix' => 'Ncm'], function () use ($router) {
//     $router->post('obterFamiliaNcm', 'NcmController@obterFamiliaNcm');
//     $router->post('pesquisaNcm', 'NcmController@pesquisaNcm');
//     $router->post('vinculoNcmCnae', 'NcmController@vinculoNcmCnae');
// });
