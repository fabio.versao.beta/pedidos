<?php

namespace App\Interfaces;

/**
 * A interface do repositório possui os métodos onde o objeto do banco
 * ainda não esta no cenário. all, find e create, em todos esses métodos
 * estamos capturando as informaçoes, elas ainda não estão conosco
 */
interface RepositoryInterface
{
    public function all();

    public function find($id);

    public function findByPedido($id);

    // public function where($field,$value);

    public function create(array $array);

    public function delete(object $obj);

    public function update(object $obj);
}
