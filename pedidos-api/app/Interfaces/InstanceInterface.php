<?php

namespace App\Interfaces;

/**
 * A interface da instância pertence ao Modelo
 * como vemos abaixo, são métodos que utilizamos o Eloquent
 * save, delete, ... são os metodos do objeto do banco
 */
interface InstanceInterface
{
    // public function all();

    public function fill(array $array);

    public function identity();

    public function update(array $array);

    public function save();

    // public function where($field,$value);

    public function delete();

    // public function find($id);

    // public function create(array $array);
}
