<?php

namespace App\Model;

use App\Interfaces\InstanceInterface;

class Carrinho extends AbstractEntity implements InstanceInterface
{
    protected $guarded = [];
    protected $primaryKey = 'id';

    public function identity()
    {
        return $this->id;
    }
}
