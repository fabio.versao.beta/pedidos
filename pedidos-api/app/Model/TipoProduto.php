<?php

namespace App\Model;

use App\Interfaces\InstanceInterface;

class TipoProduto extends AbstractEntity implements InstanceInterface
{
    protected $guarded = [];
    protected $table = 'tipo_produto';

    public function identity()
    {
        return $this->id;
    }
}
