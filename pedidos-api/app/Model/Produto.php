<?php

namespace App\Model;

use App\Interfaces\InstanceInterface;

class Produto extends AbstractEntity implements InstanceInterface
{
    protected $guarded = [];

    public function identity()
    {
        return $this->id;
    }
}
