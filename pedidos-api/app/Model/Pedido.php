<?php

namespace App\Model;

use App\Interfaces\InstanceInterface;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pedido extends AbstractEntity implements InstanceInterface
{
    use SoftDeletes;

    protected $guarded = [];

    public function identity()
    {
        return $this->id;
    }
}
