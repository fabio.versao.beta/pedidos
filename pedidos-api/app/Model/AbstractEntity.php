<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Classe abstrata de Model
 */
abstract class AbstractEntity extends Model
{
}
