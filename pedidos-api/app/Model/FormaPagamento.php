<?php

namespace App\Model;

use App\Interfaces\InstanceInterface;

class FormaPagamento extends AbstractEntity implements InstanceInterface
{
    protected $guarded = [];

    public function identity()
    {
        return $this->id;
    }
}
