<?php

namespace App\Validators;

class PagamentoValidator extends Validator
{
    /**
     * Array de regras de validação
     */
    public static $regras = [
        'pedido_id' => 'required',
        'valor_pedido' => 'required',
        'valor_pago' => 'required'
    ];
}
