<?php

namespace App\Validators;

class PedidoValidator extends Validator
{
    /**
     * Array de regras de validação
     */
    public static $regras = [
        'numero_pedido' => 'required',
        'valor_pago' => 'required',
        'forma_pagamento_id' => 'required',
        'status_id' => 'required'
    ];
}
