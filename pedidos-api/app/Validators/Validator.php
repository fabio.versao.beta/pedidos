<?php

namespace App\Validators;

use \Validator as V;
// use \Validator;
// use Illuminate\Validation\Validator;

abstract class Validator
{
    /**
     * Validation errors
     *
     * @var Illuminate\Support\MessageBag
     */
    protected $errors;

    protected static $messages = array();

    protected static $rules = array();

    /**
     * Abstracts Laravel's Validator::make()
     *
     * @param array $input Array of input data
     *
     * @return boolean
     */
    public function validate($input)
    {
        $messages = (isset(static::$messages)) ? static::$messages : array();

        $validator = V::make($input, static::$rules, $messages);

        if ($validator->fails()) {
            $this->errors = $validator->messages();
            return false;
        }

        return true;
    }

    /**
     * Getter method for the errors property
     *
     * @return Illuminate\Support\MessageBag
     */
    public function errors()
    {
        return $this->errors;
    }
}
