<?php

namespace App\Validators;

class CarrinhoValidator extends Validator
{
    /**
     * Array de regras de validação
     */
    public static $regras = [
        'produto_id' => 'required',
        'quantidade' => 'required',
        'valor' => 'required'
    ];
}
