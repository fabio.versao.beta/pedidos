<?php

namespace App\Validators;

class FinderValidator extends Validator
{
    /**
     * Array de regras de validação
     */
    public static $regras = [
        'field' => 'required',
        'value' => 'required'
    ];
}
