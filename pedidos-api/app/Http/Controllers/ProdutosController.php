<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ProdutosRepositoryInterface;
use App\Interfaces\InstanceInterface;
use App\Notification\CreatorInterface;
use Exception;
use App\Validators\Validator;

class ProdutosController extends Controller
{
    /**
     * Produtos Repository
     *
     * @var ProdutosRepositoryInterface
     */
    protected $tipoProduto;

    public function __construct(ProdutosRepositoryInterface $produtos)
    {
        $this->produtos = $produtos;
    }

    /**
     * Exibe uma lista do recurso
     */
    public function index()
    {
        try {

            $dados = $this->produtos->all();

            echo $this->formataRetorno(
                200, $dados, 1, $this->sucessoRetorno(1)
            );

        } catch (Exception $e) {

            echo $this->formataRetorno(
                500, [], 2, $this->erroRetorno(2)
            );
        }
    }
}
