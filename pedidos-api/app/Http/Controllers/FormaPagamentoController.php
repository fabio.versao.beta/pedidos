<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\FormaPagamentoRepositoryInterface;
use App\Interfaces\InstanceInterface;
use App\Notification\CreatorInterface;
use Exception;
use App\Validators\Validator;

class FormaPagamentoController extends Controller
{
    /**
     * FormaPagamento Repository
     *
     * @var FormaPagamentoRepositoryInterface
     */
    protected $formaPagamento;

    public function __construct(FormaPagamentoRepositoryInterface $formaPagamento)
    {
        $this->formaPagamento = $formaPagamento;
    }

    /**
     * Exibe uma lista do recurso
     */
    public function index()
    {
        try {

            $dados = $this->formaPagamento->all();

            echo $this->formataRetorno(
                200, $dados, 1, $this->sucessoRetorno(1)
            );

        } catch (Exception $e) {

            echo $this->formataRetorno(
                500, [], 2, $e->getMessage() //$this->erroRetorno(2)
            );
        }
    }
}
