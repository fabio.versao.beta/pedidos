<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Carrinho\EncherCarrinho;
use App\Repositories\CarrinhoRepositoryInterface;
use App\Interfaces\InstanceInterface;
use App\Notification\CreatorInterface;
use Exception;
use App\Validators\Validator;

class CarrinhoController extends Controller implements
CreatorInterface
{
    /**
     * Carrinho Repository
     *
     * @var CarrinhoRepositoryInterface
     */
    protected $carrinho;

    public function __construct(CarrinhoRepositoryInterface $carrinho)
    {
        $this->carrinho = $carrinho;
    }

    /**
     * Exibe uma lista do recurso
     */
    public function index()
    {
        try {

            $dados = $this->carrinho->all();

            echo $this->formataRetorno(
                200, $dados, 1, $this->sucessoRetorno(1)
            );

        } catch (Exception $e) {

            echo $this->formataRetorno(
                500, [], 2, $this->erroRetorno(2)
            );
        }
    }

    /**
     * Exibe uma lista do recurso
     */
    public function findByPedido($id)
    {
        try {

            $dados = $this->carrinho->findByPedido($id);

            echo $this->formataRetorno(
                200, $dados, 1, $this->sucessoRetorno(1)
            );

        } catch (Exception $e) {

            echo $this->formataRetorno(
                500, [], 2, $this->erroRetorno(2)
            );
        }
    }

    /**
     * Guarda um novo recurso criado
     */
    public function store(Request $request)
    {
        // $array = [
        //     0 => [
        //         'pedido_id' => 2,
        //         'produto_id' => 2,
        //         'tipo_produto_id' => 1,
        //         'quantidade' => 2,
        //         'valor' => 5,
        //         'observacao' => '-'
        //     ],
        //     1 => [
        //         'pedido_id' => 2,
        //         'produto_id' => 1,
        //         'tipo_produto_id' => 1,
        //         'quantidade' => 4,
        //         'valor' => 5,
        //         'observacao' => '-'
        //     ]
        // ];

        $array = (array) $request->all();

        $carrinho_criador = \App::make('App\Services\Carrinho\EncherCarrinho');

        return $carrinho_criador->create($this->carrinho,$this,$array);
    }

    /**
     * Handle successful creation
     *
     * @param  InstanceInterface $order
     * @return Redirect::route
     */
    public function creationSucceeded(InstanceInterface $instance)
    {
        return [
            'dados' => $instance,
            'message' => 'Carrinho foi enchido com sucesso'
        ];
        return 'Carrinho foi enchido com sucesso';
    }

    /**
     * Handle successful creation array
     *
     * @param  array $order
     * @return Redirect::route
     */
    public function creationSucceededArray(array $instances)
    {
        return [
            'dados' => $instances,
            'message' => 'Carrinho foi enchido com sucesso'
        ];
        return 'Carrinho foi enchido com sucesso';
    }

        /**
     * Handle successful creation array
     *
     * @param  array $order
     * @return Redirect::route
     */
    public function creationSucceededObj(object $instances)
    {
        return [
            'dados' => $instances,
            'message' => 'Operacao realizada com sucesso'
        ];
        return 'Operacao realizada com sucesso';
    }

    /**
     * Handle an error with creation
     *
     * @param  Validator\Validator      $validator
     * @return Redirect::route
     */
    public function creationFailed(Validator $validator)
    {
        return 'Oops, ocorreu um erro no carrinho';
    }
}
