<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\TipoProdutoRepositoryInterface;
use App\Interfaces\InstanceInterface;
use App\Notification\CreatorInterface;
use Exception;
use App\Validators\Validator;

class TipoProdutoController extends Controller
{
    /**
     * TipoProduto Repository
     *
     * @var TipoProdutoRepositoryInterface
     */
    protected $tipoProduto;

    public function __construct(TipoProdutoRepositoryInterface $tipoProduto)
    {
        $this->tipoProduto = $tipoProduto;
    }

    /**
     * Exibe uma lista do recurso
     */
    public function index()
    {
        try {

            $dados = $this->tipoProduto->all();

            echo $this->formataRetorno(
                200, $dados, 1, $this->sucessoRetorno(1)
            );

        } catch (Exception $e) {

            echo $this->formataRetorno(
                500, [], 2, $this->erroRetorno(2)
            );
        }
    }
}
