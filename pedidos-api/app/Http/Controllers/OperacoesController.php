<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Interfaces\InstanceInterface;
use App\Notification\CreatorInterface;
use Exception;
use App\Validators\Validator;
use App\Services\Operacoes;

class OperacoesController extends Controller implements
    CreatorInterface
{
    /**
     * Operacoes Services
     *
     */
    protected $somar;

    public function __construct()
    {
        $this->somar = \App::make('App\Services\Operacoes\SomarProdutos');
        $this->trocar = \App::make('App\Services\Operacoes\TrocoPedido');
    }

    /**
     * Soma a partir de uma lista do recurso
     */
    public function soma(Request $request)
    {
        // return $request->all();

        // $array = [
        //     0 => [
        //         'pedido_id' => 1,
        //         'produto_id' => 1,
        //         'tipo_produto_id' => 1,
        //         'quantidade' => 1,
        //         'valor' => 5,
        //         'observacao' => '-'
        //     ],
        //     1 => [
        //         'pedido_id' => 1,
        //         'produto_id' => 1,
        //         'tipo_produto_id' => 1,
        //         'quantidade' => 2,
        //         'valor' => 5,
        //         'observacao' => '-'
        //     ]
        // ];

        $array = (array) $request->all();

        try {

            $dados = $this->somar->soma($this, $array);

            echo $this->formataRetorno(
                200,
                $dados,
                1,
                $this->sucessoRetorno(1)
            );
        } catch (Exception $e) {

            echo $this->formataRetorno(
                500,
                [],
                2,
                $e->getMessage() //$this->erroRetorno(2)
            );
        }
    }

    /**
     * Calcula troco a partir de uma lista do recurso
     */
    public function troco(Request $request)
    {
        $array = [
            'pedido_id' => $request->pedido_id,
            'valor_pedido' => $request->valor_pedido,
            'valor_pago' => $request->valor_pago
        ];

        try {
            $dados = $this->trocar->troco($this, $array);

            echo $this->formataRetorno(
                200,
                $dados,
                1,
                $this->sucessoRetorno(1)
            );
        } catch (Exception $e) {

            echo $this->formataRetorno(500, [], 2, $e->getMessage());
        }
    }

    /**
     * Handle successful creation
     *
     * @param  InstanceInterface $order
     * @return Redirect::route
     */
    public function creationSucceeded(InstanceInterface $instance)
    {
        return [
            'dados' => $instance,
            'message' => 'Operacao realizada com sucesso'
        ];
        return 'Operacao realizada com sucesso';
    }

    /**
     * Handle successful creation array
     *
     * @param  array $order
     * @return Redirect::route
     */
    public function creationSucceededArray(array $instances)
    {
        return $instances;
        return [
            'dados' => $instances,
            'message' => 'Operacao realizada com sucesso'
        ];
        return 'Operacao realizada com sucesso';
    }

    /**
     * Handle successful creation array
     *
     * @param  array $order
     * @return Redirect::route
     */
    public function creationSucceededObj(object $instances)
    {
        return $instances;
        return [
            'dados' => $instances,
            'message' => 'Operacao realizada com sucesso'
        ];
        return 'Operacao realizada com sucesso';
    }

    /**
     * Handle an error with creation
     *
     * @param  Validator\Validator      $validator
     * @return Redirect::route
     */
    public function creationFailed(Validator $validator)
    {
        return 'Oops, ocorreu um erro na operacao';
    }
}
