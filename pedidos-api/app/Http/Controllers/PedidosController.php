<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Pedidos\GerarPedido;
// use Services\Pedidos\VisualizarPedido;
// use App\Services\PedidosService;
use App\Repositories\PedidoRepositoryInterface;
use App\Interfaces\InstanceInterface;
use App\Notification\CreatorInterface;
use App\Notification\UpdaterInterface;
use App\Notification\DestroyerInterface;
use App\Notification\FinderInterface;
use Exception;
use App\Validators\Validator;

class PedidosController extends Controller implements
    CreatorInterface,
    UpdaterInterface,
    DestroyerInterface,
    FinderInterface
{
    /**
     * Pedido Repository
     *
     * @var PedidoRepositoryInterface
     */
    protected $pedido;

    public function __construct(PedidoRepositoryInterface $pedido)
    {
        $this->pedido = $pedido;
    }

    /**
     * Exibe uma lista do recurso
     */
    public function index()
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 200000);

        try {

            $dados = $this->pedido->all();

            echo $this->formataRetorno(
                200,
                $dados,
                1,
                $this->sucessoRetorno(1)
            );
        } catch (Exception $e) {

            echo $this->formataRetorno(
                500,
                [],
                2,
                $e->getMessage() //$this->erroRetorno(2)
            );
        }
    }

    public function buscar_concluido()
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 200000);

        $pedido_buscar = \App::make('App\Services\Pedidos\BuscarPedidosConcluidos');

        return $pedido_buscar->buscar($this->pedido, $this);
    }

    /**
     * Exibe uma lista do recurso pelo status_id
     */
    public function buscar(Request $request)
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 200000);

        try {

            $array = (array) $request->all();

            $pedido_buscar = \App::make('App\Services\Pedidos\BuscarPedido');

            return $pedido_buscar->buscar($this->pedido, $this, $array);

            echo $this->formataRetorno(
                200,
                $array,
                1,
                $this->sucessoRetorno(1)
            );
        } catch (Exception $e) {

            echo $this->formataRetorno(
                500,
                [],
                2,
                $e->getMessage().' line: '.$e->getLine(). ' file: '.$e->getFile() //$this->erroRetorno(2)
            );
        }
    }

    /**
     * Guarda um novo recurso criado
     */
    public function store(Request $request)
    {
        $array = (array) $request->all();

        // $array = [
        //     'numero_pedido' => '1',
        //     'status_id' => 1,
        //     'forma_pagamento_id' => 1,
        //     'dinheiro_pago' => 20,
        //     'dinheiro_troco' => 10,
        //     'motivo_cancelado' => ''
        // ];

        // return json_encode($array);

        $pedido_criador = \App::make('App\Services\Pedidos\GerarPedido'); // new GerarPedido(null); // App::make('Services\Pedidos\GerarPedido');// new 'App\Services\Pedidos\GerarPedido';// resolve('App\Services\Pedidos\GerarPedido');// App::make('Services\Pedidos\GerarPedido');

        // return $array;

        return $pedido_criador->create($this->pedido, $this, $array);
    }

    /**
     * Altera o status do recurso informado
     */
    public function delivery($id)
    {
        $pedido_atualizador = \App::make('App\Services\Pedidos\EntregarPedido');

        $array = [
            'id' => $id
        ];

        return $pedido_atualizador->delivery($this->pedido, $this, $array);
    }

    /**
     * Altera o status do recurso informado
     */
    public function giveback($id)
    {
        $pedido_atualizador = \App::make('App\Services\Pedidos\DevolverPedido');

        $array = [
            'id' => $id
        ];

        return $pedido_atualizador->giveback($this->pedido, $this, $array);
    }

    /**
     * Altera o responsável do recurso informado
     */
    public function start(Request $request)
    {
        $pedido_atualizador = \App::make('App\Services\Pedidos\IniciarPedido');

        $array = (array) $request->all();

        // $array = [
        //     'id' => 1,
        //     'responsavel' => 'Teste'
        // ];

        return $pedido_atualizador->start($this->pedido, $this, $array);
    }



    /**
     * Exclui o recurso informado (softDelete)
     */
    public function destroy($id)
    {
        $pedido_deletador = \App::make('App\Services\Pedidos\DeletarPedido');

        return $pedido_deletador->destroy($this->pedido, $this, $id);
    }

    /**
     * Handle successful order creation
     *
     * @param  InstanceInterface $order
     * @return Redirect::route
     */
    public function creationSucceeded(InstanceInterface $instance)
    {
        // return Redirect::route('orders.index')->with('message', 'Order was successfully created');
        return [
            'dados' => $instance,
            'message' => 'Pedido foi criado com sucesso'
        ];
        return 'Pedido foi criado com sucesso';
    }

    /**
     * Handle successful creation array
     *
     * @param  array $order
     * @return Redirect::route
     */
    public function creationSucceededArray(array $instances)
    {
        return [
            'dados' => $instances,
            'message' => 'Operacao realizada com sucesso'
        ];
        return 'Operacao realizada com sucesso';
    }

        /**
     * Handle successful creation array
     *
     * @param  array $order
     * @return Redirect::route
     */
    public function creationSucceededObj(object $instances)
    {
        return [
            'dados' => $instances,
            'message' => 'Operacao realizada com sucesso'
        ];
        return 'Operacao realizada com sucesso';
    }

    /**
     * Handle an error with order creation
     *
     * @param  Validator\Validator      $validator
     * @return Redirect::route
     */
    public function creationFailed(Validator $validator)
    {
        // return Redirect::route('orders.create')
        //     ->withInput()
        //     ->withErrors($validator->errors())
        //     ->with('message', 'Oops, there was an error');
        return 'Oops, ocorreu um erro';
    }

    /**
     * Handle successful order update
     *
     * @param  InstanceInterface $order
     * @return Redirect::route
     */
    public function updateSucceeded(InstanceInterface $instance)
    {
        // return Redirect::route('orders.index')->with('message', 'Order was successfully created');
        return [
            'dados' => $instance,
            'message' => 'Pedido foi atualizado com sucesso'
        ];
        return 'Pedido foi atualizado com sucesso';
    }

    /**
     * Handle an error with order update
     *
     * @param  Validator\Validator      $validator
     * @return Redirect::route
     */
    public function updateFailed(Validator $validator)
    {
        return 'Oops, ocorreu um erro ao atualizar';
    }

       /**
     * Handle successful order find
     *
     * @param  InstancesInterface $orders
     * @return Redirect::route
     */
    public function findSucceeded(InstanceInterface $instance)
    {
        // return Redirect::route('orders.index')->with('message', 'Order was successfully created');
        return [
            'dados' => $instance,
            'message' => 'Pedido encontrado com sucesso'
        ];
        return 'Pedidos encontrados com sucesso';
    }

      /**
     * Handle successful order find
     *
     * @param  InstancesInterface $orders
     * @return Redirect::route
     */
    public function findSucceededArray(array $instances)
    {
        // return Redirect::route('orders.index')->with('message', 'Order was successfully created');
        return [
            'dados' => $instances,
            'message' => 'Pedidos encontrados com sucesso'
        ];
        // return 'Pedidos encontrados com sucesso';
    }

    /**
     * Handle successful order find
     *
     * @param  InstancesInterface $orders
     * @return Redirect::route
     */
    public function findSucceededJson(object $instances)
    {
        // return Redirect::route('orders.index')->with('message', 'Order was successfully created');
        return [
            'dados' => $instances,
            'message' => 'Pedidos encontrados com sucesso'
        ];
        // return 'Pedidos encontrados com sucesso';
    }

    /**
     * Handle an error with order find
     *
     * @param  Validator\Validator      $validator
     * @return Redirect::route
     */
    public function findFailed(Validator $validator)
    {
        return 'Oops, ocorreu um erro ao buscar';
    }

    /**
     * Handle successful order destroy
     *
     * @param  InstanceInterface $order
     * @return Redirect::route
     */
    public function deleteSucceeded(InstanceInterface $instance)
    {
        // return Redirect::route('orders.index')->with('message', 'Order was successfully created');
        return [
            'dados' => $instance,
            'message' => 'Pedido cancelado com sucesso'
        ];
        return 'Pedido foi cancelado com sucesso';
    }

    /**
     * Handle an error with order destroy
     *
     * @param  Validator\Validator      $validator
     * @return Redirect::route
     */
    public function deleteFailed(Validator $validator)
    {
        return 'Oops, ocorreu um erro ao cancelar';
    }

    /**
     * Mostra um recurso específico
     *
     * @param int $id
     * @return Response
     */
    // public function show($id)
    // {
    //     $pedido = $this->pedido->find($id);

    //     return $pedido;
    // }

    /**
     * Atualiza um recurso específico no banco de dados
     *
     * @param int $id
     * @return Response
     */
    // public function update($id)
    // {
    //     $pedido_atualizador = App::make('Services\Pedidos\EntregarPedido.php');

    //     return $pedido_atualizador->update(
    //         $this->pedido,
    //         $this,
    //         Input::except('_method')
    //     );
    // }
}
