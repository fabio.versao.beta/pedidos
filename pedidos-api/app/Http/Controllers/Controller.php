<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function formataRetorno($status,$dados,$codigo,$mensagem)
    {
        return json_encode([
            'status' => $status,
            'dados' => $dados,
            'codigo' => $codigo,
            'mensagem' => $mensagem
        ]);
    }

    public function sucessoRetorno($id)
    {
        $sucesso = [
            1 => 'Sucesso!'
        ];

        return $sucesso[$id];
    }

    public function erroRetorno($id)
    {
        if ($id > 2) $id = 1;
        $error = [
            1 => 'Código de Erro inválido',
            2 => 'Erro interno ou de conexão com o banco de dados'
        ];

        return $error[$id];
    }
}
