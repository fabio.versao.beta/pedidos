<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Interfaces\RepositoryInterface;


interface PedidoRepositoryInterface extends RepositoryInterface
{
    // private $model;

    public function where($field,$value);

    public function whereLimitOrder();

    // public function __construct()
    // {
    //     $this->model = new Pedido();
    // }

    // public function all()
    // {
    //     return $this->model->all();
    // }

    // public function find($id)
    // {
    //     return $this->model->findOrFail($id);
    // }

    // public function fill(array $array)
    // {
    //     return $this->model->fill($array);
    // }

    // public function create(array $array)
    // {
    //     return $this->model->create($array);
    // }
}
