<?php

namespace App\Repositories;

use App\Repositories\CarrinhoRepositoryInterface;
use App\Model\Carrinho;

class DbCarrinhoRepository extends DbRepository implements CarrinhoRepositoryInterface
{
    public function __construct(Carrinho $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        return $this->model->all();
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function findByPedido($id)
    {
        return $this->model->where('pedido_id','=',$id)->get();
    }

    public function create(array $array)
    {
        // $this->model->fill($array);
        $this->model = new Carrinho;
        $this->model->fill($array);
        $this->model->save();

        return $this->model;
    }

    public function delete(object $model)
    {
        $this->model->delete();

        return $this->model;
    }

    public function update(object $model)
    {
        $this->model = $model;
        $this->model->save();

        return $this->model;
    }

    /**
     * Pedidos excluídos (softDelete)
     */
    public function trashed()
    {
        return Carrinho::onlyTrashed();
    }
}
