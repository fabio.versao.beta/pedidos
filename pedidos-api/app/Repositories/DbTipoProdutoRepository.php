<?php

namespace App\Repositories;

use App\Repositories\TipoProdutoRepositoryInterface;
use App\Model\TipoProduto;

class DbTipoProdutoRepository extends DbRepository implements TipoProdutoRepositoryInterface
{
    public function __construct(TipoProduto $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        return $this->model->all();
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function create(array $array)
    {
        $this->model->fill($array);
        $this->model->save();

        return $this->model;
    }

    public function delete(object $model)
    {
        $this->model->delete();

        return $this->model;
    }

    public function update(object $model)
    {
        $this->model = $model;
        $this->model->save();

        return $this->model;
    }

    /**
     * Pedidos excluídos (softDelete)
     */
    public function trashed()
    {
        return TipoProduto::onlyTrashed();
    }
}
