<?php

namespace App\Repositories;

use App\Interfaces\InstanceInterface;
use App\Interfaces\RepositoryInterface;

abstract class DbRepository implements RepositoryInterface
{
    protected $model;

    public function __construct(InstanceInterface $model){
        $this->model = $model;
    }


    public function all()
    {
        return $this->model->all();
    }


    public function find($id)
    {
        return $this->model->find($id);
    }

    public function findByPedido($id)
    {
        return $this->model->where('pedido_id','=',$id);
    }

    public function where($field,$value)
    {
        return $this->model->where($field,'=',$value)->get();

    }

    public function whereLimitOrder()
    {
        return $this->model->where('status_id','=',3)->orderBy('updated_at', 'DESC')->limit(5)->get();
    }

    public function fill(array $array)
    {
        return $this->model->fill($array);
    }


    public function create(array $array)
    {
        return $this->model->create($array);
    }
}
