<?php

namespace App\Repositories;

use App\Repositories\PedidoRepositoryInterface;
use App\Model\Pedido;

class DbPedidoRepository extends DbRepository implements PedidoRepositoryInterface
{
    public function __construct(Pedido $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        return $this->model->all();
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function create(array $array)
    {
        $this->model->fill($array);
        // $this->model->status_id = 1;
        $this->model->save();

        return $this->model;
    }

    public function delete(object $model)
    {
        $this->model->delete();

        return $this->model;
    }

    public function update(object $model)
    {
        $this->model = $model;
        $this->model->save();

        return $this->model;
    }

    /**
     * Pedidos excluídos (softDelete)
     */
    public function trashed()
    {
        return Pedido::onlyTrashed();
    }
}
