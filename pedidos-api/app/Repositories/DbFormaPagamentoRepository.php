<?php

namespace App\Repositories;

use App\Repositories\FormaPagamentoRepositoryInterface;
use App\Model\FormaPagamento;

class DbFormaPagamentoRepository extends DbRepository implements FormaPagamentoRepositoryInterface
{
    public function __construct(FormaPagamento $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        return $this->model->all();
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function create(array $array)
    {
        $this->model->fill($array);
        $this->model->save();

        return $this->model;
    }

    public function delete(object $model)
    {
        $this->model->delete();

        return $this->model;
    }

    public function update(object $model)
    {
        $this->model = $model;
        $this->model->save();

        return $this->model;
    }

    /**
     * FormaPagamento excluídos (softDelete) falta configurar
     */
    public function trashed()
    {
        return FormaPagamento::onlyTrashed();
    }
}
