<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoriesServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            'App\Repositories\PedidoRepositoryInterface',
            'App\Repositories\DbPedidoRepository'
        );

        $this->app->bind(
            'App\Repositories\CarrinhoRepositoryInterface',
            'App\Repositories\DbCarrinhoRepository'
        );

        $this->app->bind(
            'App\Repositories\TipoProdutoRepositoryInterface',
            'App\Repositories\DbTipoProdutoRepository'
        );

        $this->app->bind(
            'App\Repositories\ProdutosRepositoryInterface',
            'App\Repositories\DbProdutosRepository'
        );

        $this->app->bind(
            'App\Repositories\FormaPagamentoRepositoryInterface',
            'App\Repositories\DbFormaPagamentoRepository'
        );
    }
}
