<?php

namespace App\Notification;

use App\Validators\Validator;
use App\Interfaces\InstanceInterface;

interface FinderInterface
{
    public function findSucceeded(InstanceInterface $instance);

    public function findSucceededArray(array $instances);

    public function findSucceededJson(object $instances);

    public function findFailed(Validator $validator);
}
