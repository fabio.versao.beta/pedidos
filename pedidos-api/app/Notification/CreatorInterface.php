<?php

namespace App\Notification;

use App\Validators\Validator;
use App\Interfaces\InstanceInterface;

interface CreatorInterface
{
    public function creationSucceeded(InstanceInterface $instance);

    public function creationSucceededArray(array $instances);

    public function creationSucceededObj(object $instances);

    public function creationFailed(Validator $validator);
}
