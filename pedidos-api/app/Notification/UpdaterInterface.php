<?php

namespace App\Notification;

use App\Validators\Validator;
use App\Interfaces\InstanceInterface;

interface UpdaterInterface
{
    public function updateSucceeded(InstanceInterface $instance);

    public function updateFailed(Validator $validator);
}
