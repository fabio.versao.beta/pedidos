<?php

namespace App\Notification;

use App\Validators\Validator;
use App\Interfaces\InstanceInterface;

interface DestroyerInterface
{
    public function deleteSucceeded(InstanceInterface $instance);

    public function deleteFailed(Validator $validator);
}
