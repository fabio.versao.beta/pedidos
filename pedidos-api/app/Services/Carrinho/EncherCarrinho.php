<?php

namespace App\Services\Carrinho;

use App\Repositories\CarrinhoRepositoryInterface;
use App\Notification\CreatorInterface;
use App\Validators\CarrinhoValidator;

class EncherCarrinho
{
    protected $validator;

    /**
     * Injeta o validator para usar para encher o Carrinho
     *
     * @param CarrinhoValidator $validator
     */
    public function __construct(CarrinhoValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Tentativa de encher um carrinho pegando os atributos e
     * notificando o $listener se deu sucesso ou falha
     */
    public function create(
        CarrinhoRepositoryInterface $model,
        CreatorInterface $listener,
        array $list = []
    )
    {
        // front envia um array com os itens do carrinho
        foreach ($list as $key => $attributes) {
            if ($this->validator->validate($attributes)) {

                $instancias[$key] = $model->create($attributes);
            }
        }

        if(sizeof($instancias) > 0) {
            return $listener->creationSucceededArray($instancias);
        } else {
            return $listener->creationFailed($this->validator);
        }
    }

}
