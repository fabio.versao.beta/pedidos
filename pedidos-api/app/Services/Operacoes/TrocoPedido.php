<?php

namespace App\Services\Operacoes;

use App\Notification\CreatorInterface;
use App\Validators\PagamentoValidator;

class TrocoPedido
{
    protected $validator;

    /**
     * Injeta o validator para usar para encher o Carrinho
     *
     * @param PagamentoValidator $validator
     */
    public function __construct(PagamentoValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Tentativa de calcular o valor total do carrinho
     * e notificando o $listener se deu sucesso ou falha
     */
    public function troco(
        CreatorInterface $listener,
        array $attributes = []
    )
    {
        $troco = 0;
        $resposta = [];

        if ($this->validator->validate($attributes)) {

            $troco = $attributes['valor_pago'] - $attributes['valor_pedido'];

            $resposta = [
                'pedido_id' => $attributes['pedido_id'],
                'valor_pedido' => $attributes['valor_pedido'],
                'valor_pago' => $attributes['valor_pago'],
                'troco' => $troco,
            ];
        }

        if(sizeof($resposta) > 0) {
            return $listener->creationSucceededArray($resposta);
        } else {
            return $listener->creationFailed($this->validator);
        }
    }
}
