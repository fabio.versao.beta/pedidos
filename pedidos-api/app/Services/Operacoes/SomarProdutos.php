<?php

namespace App\Services\Operacoes;

use App\Notification\CreatorInterface;
use App\Validators\CarrinhoValidator;

class SomarProdutos
{
    protected $validator;

    /**
     * Injeta o validator para usar para encher o Carrinho
     *
     * @param CarrinhoValidator $validator
     */
    public function __construct(CarrinhoValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Tentativa de calcular o valor total do carrinho
     * e notificando o $listener se deu sucesso ou falha
     */
    public function soma(
        CreatorInterface $listener,
        array $list = []
    )
    {
        $valorTotal = 0;
        $resumo = [];
        $registro = [];
        $resposta = [];
        $desconto = 0;
        $vezes = 0;
        $hamburger = 0;
        $molho = 0;


        // front envia um array com os itens do carrinho
        foreach ($list as $key => $attributes) {
            if ($this->validator->validate($attributes)) {

                // ALFAJOR 2 POR R$ 5,00
                if ($attributes['produto_id'] == 9 && $attributes['quantidade'] > 1) {
                    if ($attributes['quantidade'] % 2 == 0) {
                        $vezes = intval($attributes['quantidade'] / 2);
                        $desconto = $desconto + ($vezes * 1);
                    }
                }

                //MOLHOS
                if (intval($attributes['valor']) == 1) {
                    $molho = $molho + $attributes['quantidade'];
                }

                //HAMBURGER
                if (in_array($attributes['produto_id'],[1,2])) {
                    $hamburger = $hamburger + $attributes['quantidade'];
                }

                $valorTotal += $attributes['valor'] * $attributes['quantidade'];

                $registro = [
                    'produto_id' => $attributes['produto_id'],
                    'valor' => $attributes['valor'],
                    'quantidade' => $attributes['quantidade'],
                    'total' => ($attributes['valor'] * $attributes['quantidade'])
                ];

                $resumo[] = $registro;
            }
        }

        if ($molho > 0 && $hamburger > 0) {
            if ($hamburger >= $molho) {
                $desconto = $desconto + ($molho * 1);
            }

            if ($molho > $hamburger) {
                $desconto = $desconto + ($hamburger * 1);
            }
        }

        $resposta = [
            'resumo' => $resumo,
            'total_pedido' => ($valorTotal - $desconto),
            'desconto' => $desconto
        ];

        if($valorTotal > 0) {
            return $listener->creationSucceededArray($resposta);
        } else {
            return $listener->creationFailed($this->validator);
        }
    }
}
