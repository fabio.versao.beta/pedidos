<?php

namespace App\Services\Pedidos;

use App\Repositories\PedidoRepositoryInterface;
use App\Notification\FinderInterface;
use App\Validators\FinderValidator;

class BuscarPedidosConcluidos
{
    protected $validator;

    /**
     * Injeta o validator para usar para gerar Pedido
     *
     * @param PedidoValidator $validator
     */
    public function __construct(FinderValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Tentativa de criar um novo pedido pegando os atributos e
     * notificando o $listener se deu sucesso ou falha
     */
    public function buscar(
        PedidoRepositoryInterface $pedido,
        FinderInterface $listener
    )
    {
        $instancias = $pedido->whereLimitOrder();

        return $listener->findSucceededJson($instancias);

    }
}
