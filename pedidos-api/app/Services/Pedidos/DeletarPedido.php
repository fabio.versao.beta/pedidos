<?php

namespace App\Services\Pedidos;

use App\Repositories\PedidoRepositoryInterface;
use App\Notification\DestroyerInterface;

class DeletarPedido
{
    public function destroy(
        PedidoRepositoryInterface $pedido,
        DestroyerInterface $listener,
        $identity,
        array $attributes = []
        )
    {
        $instancia = $pedido->find($identity);

        if ($instancia->delete($instancia)) {

            return $listener->deleteSucceeded($instancia);

        } else {

            return $listener->deleteFailed($instancia);

        }
    }
}
