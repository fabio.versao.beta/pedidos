<?php

namespace App\Services\Pedidos;

use App\Repositories\PedidoRepositoryInterface;
use App\Notification\UpdaterInterface;
use App\Validators\PedidoValidator;

class EntregarPedido
{
    protected $validator;

    /**
     * Injeta o validator para usar para gerar Pedido
     *
     * @param PedidoValidator $validator
     */
    public function __construct(PedidoValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Tentativa de criar um novo pedido pegando os atributos e
     * notificando o $listener se deu sucesso ou falha
     */
    public function delivery(
        PedidoRepositoryInterface $pedido,
        UpdaterInterface $listener,
        array $attributes = []
    )
    {
        if ($this->validator->validate($attributes)) {

            $instancia = $pedido->find($attributes['id']);
            $instancia->status_id = 3;
            $instancia = $pedido->update($instancia);

            return $listener->updateSucceeded($instancia);

        } else {

            return $listener->updateFailed($this->validator);
        }
    }

}
