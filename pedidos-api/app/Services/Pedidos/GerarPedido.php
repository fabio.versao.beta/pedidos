<?php

namespace App\Services\Pedidos;

use App\Repositories\PedidoRepositoryInterface;
use App\Notification\CreatorInterface;
// use Contracts\Repositories\CarrinhoRepositoryInterface;
use App\Validators\PedidoValidator;

class GerarPedido
{
    protected $validator;

    /**
     * Injeta o validator para usar para gerar Pedido
     *
     * @param PedidoValidator $validator
     */
    public function __construct(PedidoValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Tentativa de criar um novo pedido pegando os atributos e
     * notificando o $listener se deu sucesso ou falha
     */
    public function create(
        PedidoRepositoryInterface $pedido,
        CreatorInterface $listener,
        array $attributes = []
    )
    {
        if ($this->validator->validate($attributes)) {

            $instancia = $pedido->create($attributes);

            return $listener->creationSucceeded($instancia);

        } else {

            return $listener->creationFailed($this->validator);
        }
    }

}
