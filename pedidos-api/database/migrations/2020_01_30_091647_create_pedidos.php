<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numero_pedido', 30)->nullable();
            $table->integer('status_id')->unsigned();
            $table->integer('forma_pagamento_id')->unsigned();
            $table->foreign('forma_pagamento_id')->references('id')->on('forma_pagamentos');
            $table->decimal('dinheiro_pago', 8, 2)->nullable();
            $table->decimal('dinheiro_troco', 8, 2)->nullable();
            $table->string('motivo_cancelado', 144)->nullable();
            $table->string('nome_cliente', 100)->nullable();
            $table->string('telefone', 20)->nullable();
            $table->string('nome_caixa', 100)->nullable();
            $table->string('responsavel', 100)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->index(['numero_pedido','status_id','responsavel']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidos');
    }
}
