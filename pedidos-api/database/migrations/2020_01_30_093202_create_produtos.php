<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdutos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produtos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tipo_produto_id')->unsigned();
            $table->foreign('tipo_produto_id')->references('id')->on('tipo_produto');
            $table->string('nome', 200)->nullable();
            $table->string('imagem', 400)->nullable();
            $table->decimal('valor', 8, 2);
            $table->integer('ativo')->nullable();
            $table->timestamps();
            $table->index(['nome','valor','tipo_produto_id']);
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produtos');
    }
}
