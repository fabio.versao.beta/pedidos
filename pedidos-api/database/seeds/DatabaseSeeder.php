<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     * php artisan migrate:fresh --seed
     *
     * @return void
     */
    public function run()
    {
        $this->call(FormaPagamentoSeeder::class);
        $this->call(StatusSeeder::class);
        $this->call(TipoProdutoSeeder::class);
        $this->call(ProdutosSeeder::class);
    }
}
