<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status')->insert([
            'id' => 1,
            'descricao' => 'Pago',
            'ativo' => 1
        ]);

        DB::table('status')->insert([
            'id' => 2,
            'descricao' => 'Preparando',
            'ativo' => 1
        ]);

        DB::table('status')->insert([
            'id' => 3,
            'descricao' => 'Finalizado',
            'ativo' => 1
        ]);

        DB::table('status')->insert([
            'id' => 4,
            'descricao' => 'Cancelado',
            'ativo' => 1
        ]);
    }
}
