<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ProdutosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('produtos')->insert([
            'nome' => 'Hamburguer de Carne',
            'valor' => 5,
            'tipo_produto_id' => 1,
            'imagem' => 'hamburger2.png',
            'ativo' => 1
        ]);

        DB::table('produtos')->insert([
            'nome' => 'Hamburguer Vegetariano',
            'valor' => 5,
            'tipo_produto_id' => 1,
            'imagem' => 'hamburger2.png',
            'ativo' => 0
        ]);

        DB::table('produtos')->insert([
            'nome' => 'Coca-Cola',
            'valor' => 3,
            'tipo_produto_id' => 2,
            'imagem' => 'refri-coca.png',
            'ativo' => 1
        ]);

        DB::table('produtos')->insert([
            'nome' => 'Fanta',
            'valor' => 3,
            'tipo_produto_id' => 2,
            'imagem' => 'refri-fanta.png',
            'ativo' => 1
        ]);

        DB::table('produtos')->insert([
            'nome' => 'Sprite',
            'valor' => 3,
            'tipo_produto_id' => 2,
            'imagem' => 'refri-sprite.jpg',
            'ativo' => 1
        ]);

        DB::table('produtos')->insert([
            'nome' => 'Fanta Uva',
            'valor' => 3,
            'tipo_produto_id' => 2,
            'imagem' => 'refri-uva.png',
            'ativo' => 1
        ]);

        DB::table('produtos')->insert([
            'nome' => 'Kuat',
            'valor' => 3,
            'tipo_produto_id' => 2,
            'imagem' => 'refri-kuat.jpg',
            'ativo' => 1
        ]);

        // DB::table('produtos')->insert([
        //     'nome' => 'Guaraná',
        //     'valor' => 3,
        //     'tipo_produto_id' => 2,
        //     'imagem' => '',
        //     'ativo' => 1
        // ]);

        DB::table('produtos')->insert([
            'nome' => 'Agua',
            'valor' => 2,
            'tipo_produto_id' => 2,
            'imagem' => 'agua.png',
            'ativo' => 1
        ]);

        DB::table('produtos')->insert([
            'nome' => 'Alfajor',
            'valor' => 3,
            'tipo_produto_id' => 3,
            'imagem' => 'alfajor.png',
            'ativo' => 1
        ]);

        DB::table('produtos')->insert([
            'nome' => 'Beringela doidona',
            'valor' => 1,
            'tipo_produto_id' => 4,
            'imagem' => 'molho.png',
            'ativo' => 1
        ]);

        DB::table('produtos')->insert([
            'nome' => 'Maionese com Limao azedo',
            'valor' => 1,
            'tipo_produto_id' => 4,
            'imagem' => 'molho.png',
            'ativo' => 1
        ]);

        DB::table('produtos')->insert([
            'nome' => 'Barbecue',
            'valor' => 1,
            'tipo_produto_id' => 4,
            'imagem' => 'molho.png',
            'ativo' => 1
        ]);

        DB::table('produtos')->insert([
            'nome' => 'Alho',
            'valor' => 1,
            'tipo_produto_id' => 4,
            'imagem' => 'molho.png',
            'ativo' => 1
        ]);

    }
}
