<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class TipoProdutoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_produto')->insert([
            'id' => 1,
            'descricao' => 'Alimento',
            'ativo' => 1
        ]);

        DB::table('tipo_produto')->insert([
            'id' => 2,
            'descricao' => 'Bebida',
            'ativo' => 1
        ]);

        DB::table('tipo_produto')->insert([
            'id' => 3,
            'descricao' => 'Sobremesa',
            'ativo' => 1
        ]);

        DB::table('tipo_produto')->insert([
            'id' => 4,
            'descricao' => 'Molho',
            'ativo' => 1
        ]);
    }
}
