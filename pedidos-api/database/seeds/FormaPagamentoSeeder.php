<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FormaPagamentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('forma_pagamentos')->insert([
            'descricao' => 'Dinheiro',
            'ativo' => 1
        ]);

        DB::table('forma_pagamentos')->insert([
            'descricao' => 'Cartao Credito',
            'ativo' => 1
        ]);

        DB::table('forma_pagamentos')->insert([
            'descricao' => 'Cartao Debito',
            'ativo' => 1
        ]);

        DB::table('forma_pagamentos')->insert([
            'descricao' => 'PicPay',
            'ativo' => 1
        ]);

        DB::table('forma_pagamentos')->insert([
            'descricao' => 'Fiado',
            'ativo' => 1
        ]);
    }
}
