import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Receber from '../views/Receber.vue'
import Gerar from '../views/Gerar.vue'
import Acompanhar from '../views/Acompanhar.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/receber',
    name: 'Receber',
    component: Receber
  },
  {
    path: '/gerar',
    name: 'Gerar',
    component: Gerar
  },
  {
    path: '/acompanhar',
    name: 'Acompanhar',
    component: Acompanhar
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
